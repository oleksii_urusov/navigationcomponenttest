package com.example.oleksiiurusov.navigationcomponenttest.main

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import com.example.oleksiiurusov.navigationcomponenttest.common.BaseNavFragment
import com.example.oleksiiurusov.navigationcomponenttest.R
import kotlinx.android.synthetic.main.fragment_history.*

class HistoryFragment: BaseNavFragment() {
    override val layoutId: Int = R.layout.fragment_history

    override fun initUi(view: View, bundle: Bundle?) {
        btn_settings.setOnClickListener {
            navigate(HistoryFragmentDirections.actionOpenSettings())
        }

        tv_content_placeholder.setOnClickListener {
            val action = MainFragmentDirections.actionGlobalOpenDialog().apply {
                setTitle("History Fragment")
                setMessage(getString(R.string.title_history))
            }
            navigate(action)
        }
    }
}