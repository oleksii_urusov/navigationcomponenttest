package com.example.oleksiiurusov.navigationcomponenttest.login


import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import com.example.oleksiiurusov.navigationcomponenttest.common.BaseNavFragment

import com.example.oleksiiurusov.navigationcomponenttest.R
import com.example.oleksiiurusov.navigationcomponenttest.common.setAuthorized
import com.example.oleksiiurusov.navigationcomponenttest.main.MainActivity
import kotlinx.android.synthetic.main.fragment_login_step_two.*

private const val ARG_EMAIL = "email"

class LoginFragmentStepTwo : BaseNavFragment() {

    override fun initUi(view: View, bundle: Bundle?) {
        val email = LoginFragmentStepTwoArgs.fromBundle(arguments).email
        tv_email.text = email

        btn_authenticate.setOnClickListener { authenticate() }
    }

    private fun authenticate() {
        navToActivityWSharedElements(
            iv_calendar,
            "transition_calendar_image_home",
            R.id.action_authenticate,
            true) {
            it.setAuthorized(true)
        }

    }

    override val layoutId: Int = R.layout.fragment_login_step_two

}
