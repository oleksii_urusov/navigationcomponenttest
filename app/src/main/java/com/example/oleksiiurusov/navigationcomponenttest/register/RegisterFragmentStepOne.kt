package com.example.oleksiiurusov.navigationcomponenttest.register

import android.os.Bundle
import android.view.View
import com.example.oleksiiurusov.navigationcomponenttest.common.BaseNavFragment
import com.example.oleksiiurusov.navigationcomponenttest.R
import kotlinx.android.synthetic.main.fragment_register_step_one.*

class RegisterFragmentStepOne : BaseNavFragment() {
    override fun initUi(view: View, bundle: Bundle?) {
        btn_proceed.setOnClickListener {
            navigate(RegisterFragmentStepOneDirections.actionProceed())
        }
    }

    override val layoutId: Int = R.layout.fragment_register_step_one
}