package com.example.oleksiiurusov.navigationcomponenttest.main

import android.os.Bundle
import android.view.View
import com.example.oleksiiurusov.navigationcomponenttest.common.BaseNavFragment
import com.example.oleksiiurusov.navigationcomponenttest.R
import kotlinx.android.synthetic.main.fragment_subscriptions.*

class SubscriptionsFragment: BaseNavFragment() {

    override fun initUi(view: View, bundle: Bundle?) {
        tv_title.setOnClickListener {
            val action = MainFragmentDirections.actionGlobalOpenDialog().apply {
                setTitle("Subscriptions Fragment")
                setMessage(getString(R.string.title_subscriptions))
            }
            navigate(action)
        }
    }

    override val layoutId: Int = R.layout.fragment_subscriptions
}