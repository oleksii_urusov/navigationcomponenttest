package com.example.oleksiiurusov.navigationcomponenttest.main

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import com.example.oleksiiurusov.navigationcomponenttest.common.BaseNavFragment
import com.example.oleksiiurusov.navigationcomponenttest.R
import com.example.oleksiiurusov.navigationcomponenttest.common.setAuthorized
import kotlinx.android.synthetic.main.fragment_settings.*

class SettingsFragment: BaseNavFragment() {
    override fun initUi(view: View, bundle: Bundle?) {
        btn_apply.setOnClickListener {
            navController.navigate(SettingsFragmentDirections.actionReturnToMainScreen())
        }

        btn_logout.setOnClickListener {
            navigateAndFinishActivity(SettingsFragmentDirections.actionLogout()) { parentActivity ->
                parentActivity.setAuthorized(false)
            }

        }
    }

    override val layoutId: Int = R.layout.fragment_settings
}
