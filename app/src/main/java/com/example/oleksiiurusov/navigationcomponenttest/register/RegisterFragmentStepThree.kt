package com.example.oleksiiurusov.navigationcomponenttest.register

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import com.example.oleksiiurusov.navigationcomponenttest.common.BaseNavFragment
import com.example.oleksiiurusov.navigationcomponenttest.R
import kotlinx.android.synthetic.main.fragment_register_step_three.*

class RegisterFragmentStepThree: BaseNavFragment() {
    override fun initUi(view: View, bundle: Bundle?) {
        //get name from deeplink
        tv_name.text = arguments?.getString("name")

        btn_to_reg_step_one.setOnClickListener {
            navigate(RegisterFragmentStepThreeDirections.actionReturnToStepOne())
        }

        btn_proceed.setOnClickListener {
            navigate(RegisterFragmentStepThreeDirections.actionReturnToLogin())
        }
    }

    override val layoutId: Int = R.layout.fragment_register_step_three
}