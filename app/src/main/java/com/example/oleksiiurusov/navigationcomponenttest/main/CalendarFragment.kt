package com.example.oleksiiurusov.navigationcomponenttest.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.transition.ChangeBounds
import com.example.oleksiiurusov.navigationcomponenttest.common.BaseNavFragment
import com.example.oleksiiurusov.navigationcomponenttest.R

class CalendarFragment : BaseNavFragment(){

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        sharedElementEnterTransition = ChangeBounds().apply { duration = 350 }
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun initUi(view: View, bundle: Bundle?) {
    }

    override val layoutId: Int = R.layout.fragment_calendar
}