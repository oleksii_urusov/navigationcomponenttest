package com.example.oleksiiurusov.navigationcomponenttest.register

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import com.example.oleksiiurusov.navigationcomponenttest.common.BaseNavFragment
import com.example.oleksiiurusov.navigationcomponenttest.R
import kotlinx.android.synthetic.main.fragment_register_step_two.*

class RegisterFragmentStepTwo  : BaseNavFragment() {
    override fun initUi(view: View, bundle: Bundle?) {
        btn_proceed.setOnClickListener {
            navigate(RegisterFragmentStepTwoDirections.actionProceed().setName("From Step Two"))
        }
    }

    override val layoutId: Int = R.layout.fragment_register_step_two
}