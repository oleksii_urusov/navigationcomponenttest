package com.example.oleksiiurusov.navigationcomponenttest.main

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import com.example.oleksiiurusov.navigationcomponenttest.common.BaseNavFragment
import com.example.oleksiiurusov.navigationcomponenttest.R
import kotlinx.android.synthetic.main.fragment_main.*

class MainFragment : BaseNavFragment() {

    override fun initUi(view: View, bundle: Bundle?) {
        btn_history.setOnClickListener {
            navigate(MainFragmentDirections.actionOpenHistory())
        }

        btn_settings.setOnClickListener {
            navigate(MainFragmentDirections.actionOpenSettings())
        }

        iv_calendar.setOnClickListener {
            val extras = FragmentNavigatorExtras(
                btn_settings to "transition_settings_btn_calendar",
                btn_history to "transition_history_btn_calendar",
                iv_calendar to "transition_calendar_image_calendar")

            navigate(
                R.id.action_open_calendar,
                null,
                null,
                extras)
        }

        tv_title.setOnClickListener {
            val action = MainFragmentDirections.actionGlobalOpenDialog().apply {
                setTitle("Main Fragment")
                setMessage(getString(R.string.title_main_fragment))
            }
            navigate(action)
        }
    }

    override val layoutId: Int = R.layout.fragment_main
}
