package com.example.oleksiiurusov.navigationcomponenttest.custom_destination

import android.os.Bundle
import androidx.fragment.app.FragmentManager
import androidx.navigation.NavDestination
import androidx.navigation.NavOptions
import androidx.navigation.Navigator
import com.example.oleksiiurusov.navigationcomponenttest.common.InfoDialog

@Navigator.Name("info_dialog")
class InfoDialogNavigator(private val fragmentManager: FragmentManager) : Navigator<InfoDialogNavigator.Destination>(){
    override fun navigate(destination: Destination, args: Bundle?, navOptions: NavOptions?, navigatorExtras: Extras?) {
        val dialog = InfoDialog.newInstance(args)
        dialog.show(fragmentManager, InfoDialog.TAG)
    }

    override fun createDestination(): Destination {
        return Destination(this)
    }

    override fun popBackStack(): Boolean {
        return false
    }

    class Destination(infoDialogNavigator: InfoDialogNavigator) : NavDestination(infoDialogNavigator) {

    }
}