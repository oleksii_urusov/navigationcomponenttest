package com.example.oleksiiurusov.navigationcomponenttest.login

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.setupActionBarWithNavController
import com.example.oleksiiurusov.navigationcomponenttest.R
import com.example.oleksiiurusov.navigationcomponenttest.common.BaseNavActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseNavActivity() {
    override val navHostFragmentId: Int = R.id.my_nav_host_fragment
    override val layoutId: Int = R.layout.activity_login
    override val menuResId: Int? = null
    override val navGraphId: Int = R.navigation.login_navigation_graph

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initActionBar(toolbar, true)
    }

    override fun initUi(savedInstanceState: Bundle?) {}
}
