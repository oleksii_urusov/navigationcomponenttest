package com.example.oleksiiurusov.navigationcomponenttest.main

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.onNavDestinationSelected
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.oleksiiurusov.navigationcomponenttest.R
import com.example.oleksiiurusov.navigationcomponenttest.common.BaseNavActivity
import com.example.oleksiiurusov.navigationcomponenttest.common.isAuthorized
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseNavActivity() {
    override val navHostFragmentId: Int = R.id.main_nav_host_fragment
    override val layoutId: Int = R.layout.activity_main
    override val menuResId: Int? = R.menu.toolbar_menu
    override val navGraphId: Int = R.navigation.main_navigation_graph

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initActionBar(toolbar, true)
        bindBottomNavToNavController(bottom_nav_view)
    }

    override fun initUi(savedInstanceState: Bundle?) {}

    override fun onResume() {
        super.onResume()
        if(!isAuthorized()) {
            navigate(MainFragmentDirections.actionMoveToLoginActivity())
            finish()
        }
    }
}
