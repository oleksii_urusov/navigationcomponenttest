package com.example.oleksiiurusov.navigationcomponenttest.main

import android.os.Bundle
import android.view.View
import com.example.oleksiiurusov.navigationcomponenttest.R
import com.example.oleksiiurusov.navigationcomponenttest.common.BaseFragment

private const val ARG_PAGE_NUMBER = "ARG_PAGE_NUMBER"

class ViewPagerPageFragment: BaseFragment() {
    private var pageNum = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            this.pageNum = it.getInt(ARG_PAGE_NUMBER, 0)
        }
    }

    override val layoutId: Int = when(pageNum) {
        0 -> R.layout.fragment_page_zero
        1 -> R.layout.fragment_page_one
        else -> 0
    }

    override fun initUi(view: View, bundle: Bundle?) { }

    companion object {
        fun newInstance(pageNumber: Int) = ViewPagerPageFragment().apply {
            arguments = Bundle().apply {
                putInt(ARG_PAGE_NUMBER, pageNumber)
            }
        }
    }
}