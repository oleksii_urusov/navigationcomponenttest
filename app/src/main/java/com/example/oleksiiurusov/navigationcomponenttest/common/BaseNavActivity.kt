package com.example.oleksiiurusov.navigationcomponenttest.common

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.annotation.IdRes
import androidx.appcompat.widget.Toolbar
import androidx.navigation.NavDirections
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.onNavDestinationSelected
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.oleksiiurusov.navigationcomponenttest.R
import com.example.oleksiiurusov.navigationcomponenttest.custom_destination.InfoDialogNavigator
import com.google.android.material.bottomnavigation.BottomNavigationView

abstract class BaseNavActivity : BaseActivity() {
    protected abstract val navHostFragmentId: Int
    protected abstract val menuResId: Int?
    protected abstract val navGraphId: Int
    val navController by lazy { findNavController(navHostFragmentId) }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val navHostFragment = supportFragmentManager.findFragmentById(navHostFragmentId)
        navHostFragment?.childFragmentManager?.let {
            //Add "info_dialog" destination type to navController and set the graph (we need to add custom destionation
            //at first and only then we can set graph)
            val destination = InfoDialogNavigator(it)
            navHostFragment.findNavController().apply {
                navigatorProvider.addNavigator(destination)
                setGraph(navGraphId)
            }
        } ?: throw IllegalStateException("NavHostFragment cannot be null")
    }

    protected fun initActionBar(toolbar: Toolbar, bindToNavController: Boolean) {
        setSupportActionBar(toolbar)
        if (bindToNavController) { setupActionBarWithNavController(navController) }
    }

    protected fun bindBottomNavToNavController(bottomNavView: BottomNavigationView?) {
        bottomNavView?.setupWithNavController(navController)
    }


    protected fun navigate(where: NavDirections) {
        navController.navigate(where)
    }

    protected fun navigate(@IdRes where: Int,
                           args: Bundle? = null,
                           navOptions: NavOptions? = null,
                           navigatorExtras: FragmentNavigator.Extras? = null) {
        navController.navigate(where, args, navOptions, navigatorExtras)
    }

    override fun onSupportNavigateUp(): Boolean = navController.navigateUp()

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuResId?.let {
            menuInflater.inflate(it, menu)
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?) =
        (item?.onNavDestinationSelected(navController) ?: false) || super.onOptionsItemSelected(item)
}