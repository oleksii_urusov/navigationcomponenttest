package com.example.oleksiiurusov.navigationcomponenttest.main

import android.os.Bundle
import android.view.View
import com.example.oleksiiurusov.navigationcomponenttest.common.BaseNavFragment
import com.example.oleksiiurusov.navigationcomponenttest.R

class AboutFragment: BaseNavFragment() {

    override fun initUi(view: View, bundle: Bundle?) {
    }

    override val layoutId: Int = R.layout.fragment_about
}