package com.example.oleksiiurusov.navigationcomponenttest.common

import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.annotation.IdRes
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import androidx.fragment.app.FragmentActivity
import androidx.navigation.ActivityNavigator
import androidx.navigation.NavDirections
import androidx.navigation.NavOptions
import androidx.navigation.Navigator
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.findNavController

abstract class BaseNavFragment : BaseFragment() {
    val navController by lazy { findNavController() }

    protected fun navigate(@IdRes where: Int,
        args: Bundle? = null,
        navOptions: NavOptions? = null,
        navigatorExtras: FragmentNavigator.Extras? = null) {
        navController.navigate(where, args, navOptions, navigatorExtras)
    }

    protected fun navigate(where: NavDirections) {
        navController.navigate(where)
    }

    protected inline fun navigateAndFinishActivity(where: NavDirections, alsoDo: (FragmentActivity) -> Unit = {}) {
        activity?.let {
            alsoDo(it)
            navController.navigate(where)
            it.finish()
        } ?: throw IllegalArgumentException("Activity cannot be null")
    }

    protected fun navToActivityWSharedElements(
        sharedView: View,
        transitionName: String,
        @IdRes action: Int,
        finishCurrentActivity: Boolean,
        alsoDo: (FragmentActivity) -> Unit = {}
    ) {
        activity?.let{
            alsoDo(it)
            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                it,
                Pair.create(sharedView, transitionName))
            val extras = ActivityNavigator.Extras(options)

            navController.navigate(
                action,
                null,
                null,
                extras)
            if(finishCurrentActivity) {
                it.finish()
            }
        } ?: throw IllegalArgumentException("Activity cannot be null")
    }
}