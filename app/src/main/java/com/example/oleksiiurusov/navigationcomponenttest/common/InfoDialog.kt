package com.example.oleksiiurusov.navigationcomponenttest.common

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.example.oleksiiurusov.navigationcomponenttest.R

private const val ARG_MESSAGE = "message"
private const val ARG_TITLE = "title"

class InfoDialog : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        return activity?.let { it ->
            val title = arguments?.getString(ARG_TITLE)
            val message = arguments?.getString(ARG_MESSAGE)

            AlertDialog.Builder(activity)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok") { _, _ -> dismiss() }
                .create()

            /*val builder = AlertDialog.Builder(it, R.style.Theme_AppCompat_Dialog)
                .setMessage(message)
                .setTitle(title)
                .setPositiveButton("OK") { _, _ ->
                    dismiss()
                }
                builder.create()*/
        } ?: throw IllegalStateException("Activity cannot be null")
    }


    companion object {
        fun newInstance(title: String = "", message: String = ""): InfoDialog {
            return InfoDialog().apply {
                arguments = Bundle().apply {
                    putString(ARG_TITLE, title)
                    putString(ARG_MESSAGE, message)
                }
            }
        }

        fun newInstance(args: Bundle?): InfoDialog {
            return InfoDialog().apply {
                arguments = args
            }
        }

        const val TAG = "InfoDialog"
    }
}
