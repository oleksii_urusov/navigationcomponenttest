package com.example.oleksiiurusov.navigationcomponenttest.common

import android.content.Context
import android.content.Context.MODE_PRIVATE

private const val DEFAULT_SHARED_PREFS = "com.example.oleksiiurusov.navigationcomponenttest.SHARED_PREFS"
private const val PREF_AUTHORIZED = "PREF_AUTHORIZED"

fun Context.isAuthorized() = sharedPrefs().getBoolean(PREF_AUTHORIZED, false)

fun Context.setAuthorized(authorized: Boolean) {
    sharedPrefs().edit().putBoolean(PREF_AUTHORIZED, authorized).apply()
}

private fun Context.sharedPrefs() = getSharedPreferences(DEFAULT_SHARED_PREFS, MODE_PRIVATE)
