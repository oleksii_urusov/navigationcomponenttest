package com.example.oleksiiurusov.navigationcomponenttest.custom_destination

import android.os.Bundle
import android.widget.Button
import androidx.navigation.findNavController
import com.example.oleksiiurusov.navigationcomponenttest.R
import com.example.oleksiiurusov.navigationcomponenttest.common.BaseActivity

class CustomDestinationActivity: BaseActivity() {

    private val navController by lazy { findNavController(R.id.container) }
    override val layoutId: Int = R.layout.activity_custom_destination

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        navController.addOnNavigatedListener { _, destination ->
            when (destination.id) {
                R.id.destination_zero -> {
                    supportActionBar?.setDisplayHomeAsUpEnabled(false)
                    findViewById<Button>(R.id.btn_proceed).setOnClickListener {
                        navController.navigate(R.id.destination_one)
                    }
                }
                R.id.destination_one -> {
                    supportActionBar?.setDisplayHomeAsUpEnabled(true)
                    findViewById<Button>(R.id.btn_proceed).setOnClickListener {
                        navController.navigate(R.id.destination_two)
                    }
                }
                R.id.destination_two -> {
                    supportActionBar?.setDisplayHomeAsUpEnabled(true)
                    findViewById<Button>(R.id.btn_proceed).setOnClickListener {
                        navController.navigate(R.id.login_fragment_step_one)
                    }
                }
            }
        }
    }

    override fun initUi(savedInstanceState: Bundle?) {}

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp()
    }
    override fun onBackPressed() {
        if (!navController.popBackStack()) {
            super.onBackPressed()
        }
    }
}