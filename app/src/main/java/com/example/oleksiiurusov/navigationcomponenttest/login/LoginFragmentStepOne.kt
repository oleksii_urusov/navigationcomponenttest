package com.example.oleksiiurusov.navigationcomponenttest.login


import android.os.Bundle
import android.view.*
import androidx.navigation.fragment.findNavController
import com.example.oleksiiurusov.navigationcomponenttest.common.BaseNavFragment

import com.example.oleksiiurusov.navigationcomponenttest.R
import kotlinx.android.synthetic.main.fragment_login_step_one.*

class LoginFragmentStepOne : BaseNavFragment() {

    override fun initUi(view: View, bundle: Bundle?) {
        btn_login.setOnClickListener {
            val action = LoginFragmentStepOneDirections.actionProceed()
            action.setEmail(et_email.text.toString())
            navigate(action)
        }

        btn_register.setOnClickListener {
            navigate(LoginFragmentStepOneDirections.actionRegister())
        }
    }

    override val layoutId: Int = R.layout.fragment_login_step_one
}
